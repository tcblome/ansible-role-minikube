---
# debian tasks file for ansible-role-minikube

- name: Load distro-specific variables
  include_vars: "{{ item }}"
  with_first_found:
    - debian.yml

- name: Check specified minikube package is installed
  vars:
    pattern: >-
      v{{ minikube_version }}
  shell: >
    /usr/local/bin/minikube version 2>/dev/null
    | awk -F ': ' '{ print $2 }'
  args:
    executable: /bin/bash
  register: minikube_checked
  failed_when: minikube_checked.stdout != pattern
  changed_when: no
  ignore_errors: yes

- block:
  - name: Ensure minikube signature files are downloaded
    become: yes
    become_user: root
    become_method: sudo
    get_url:
      url: "https://storage.googleapis.com/minikube/releases/v{{ minikube_version }}/minikube-linux-amd64.sha256"
      dest: "/tmp/minikube-linux-amd64.sha256"
      force: yes

  - name: Read minikube hash
    command: >
      cat /tmp/minikube-linux-amd64.sha256
    register: minikube_hash

  - name: Ensure specified minikube package is downloaded and installed
    become: yes
    become_user: root
    become_method: sudo
    get_url:
      url: "https://storage.googleapis.com/minikube/releases/v{{ minikube_version }}/minikube-linux-amd64"
      checksum: "sha256:{{ minikube_hash.stdout }}"
      dest: "/usr/local/bin/minikube"
      mode: +x
  when: (minikube_checked.failed == true)

- name: Check minikube.sh profile is present
  stat:
    path: /etc/profile.d/minikube.sh
  register: minikube_sh

- name: Ensure minikube.sh profile is present
  become: yes
  become_user: root
  become_method: sudo
  file:
    path: /etc/profile.d/minikube.sh
    state: touch
  when: not minikube_sh.stat.exists

- name: Ensure minikube variables are in environment
  become: yes
  become_user: root
  become_method: sudo
  blockinfile:
    path: /etc/profile.d/minikube.sh
    block: |
      export {{ item.key }}={{ item.value }}
    marker: "# {mark} ANSIBLE MANAGED BLOCK {{ item.key }}"
  loop:
    - { key: "MINIKUBE_WANTUPDATENOTIFICATION", value: 'false' }
    - { key: "MINIKUBE_WANTREPORTERRORPROMPT", value: 'false' }
    - { key: "MINIKUBE_HOME", value: "$HOME" }
    - { key: "CHANGE_MINIKUBE_NONE_USER", value: 'true' }

- name: Ensure latest socat is installed
  become: yes
  become_user: root
  become_method: sudo
  apt:
    pkg: socat
    state: latest
    update_cache: yes
