Ansible role: minikube
=========

![pipeline](https://gitlab.com/tcblome/ansible-role-minikube/badges/master/build.svg)

This role installs [kubernetes minikube](https://kubernetes.io/docs/tasks/tools/install-minikube/).


Requirements
------------
none

Installation
------------

Using `ansible-galaxy`:

```shell
$ ansible-galaxy install https://gitlab.com/tcblome/ansible-role-minikube.git
```

Using `requirements.yml`:
```yaml
 src: git+https://gitlab.com/tcblome/ansible-role-minikube.git
 name: tcblome.minikube
```
Using `git`:
```shell
$ git clone https://gitlab.com/tcblome/ansible-role-minikube.git tcblome.minikube
```

Role Variables
--------------
```yaml

# defaults file for ansible-role-minikube
#
# minikube_version	*	# version of minikube to install

```

Dependencies
------------

This role can be used independently.

Example Playbook
----------------

Sample :
```
    - hosts: servers
      roles:
        - { role: tcblome.minikube,
	         minikube_version: "0.26.0"
	        }
```

License
------------------
[LICENSE](LICENSE)

Author Information
------------------

This role is published for private use by @tcblome
